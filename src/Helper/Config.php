<?php
/**
 * Class Helper\Config
 *
 * @category Monogo
 * @package Monogo\CustomApiClient
 * @author Łukasz Zabielski <lukasz.zabielski@monogo.pl>
 */

namespace monogo\CustomApiClient\Helper;

class Config
{
    const LOGIN_METHOD_NAME = 'apiLogin';

    const REQUEST_TYPE_JSON = 'json';
    const REQUEST_TYPE_FORM_PARAMS = 'form_params';

    const RESPONSE_TYPE_JSON = 'json';
    const RESPONSE_TYPE_XML = 'xml';

    const ENVIRONMENT_PROD = 'prod';
    const ENVIRONMENT_DEV = 'dev';

    private static $user;
    private static $password;
    private static $baseUrlDev;
    private static $baseUrlProd;
    private static $apiVersion;
    private static $environment;
    private static $responseType;
    private static $requestType;
    private static $restApiUrl;
    private static $basicAuthUsername;
    private static $basicAuthPassword;
    private static $settingsPath = null;

    /**
     * Request method default POST.
     * @var string
     */
    protected static $requestMethod = 'POST';

    /**
     * Init settings.
     * @throws \Exception
     */
    protected static function init()
    {
        if ( is_null(self::$settingsPath) ) {
            throw new \Exception('Need to init settings path.');
        }

        $clientConfig = self::getClientConfig();

        if ( isset($clientConfig['base_url_dev']) && isset($clientConfig['base_url_prod'])
            && isset($clientConfig['user']) && isset($clientConfig['password'])
            && isset($clientConfig['response_type']) && isset($clientConfig['api_version'])
            && isset($clientConfig['environment']) && isset($clientConfig['request_type'])
            && isset($clientConfig['basic_auth_username']) && isset($clientConfig['basic_auth_password'])
            && ( $clientConfig['response_type'] == self::RESPONSE_TYPE_JSON || $clientConfig['response_type'] == self::RESPONSE_TYPE_XML )
            && ( $clientConfig['environment'] == self::ENVIRONMENT_DEV || $clientConfig['environment'] == self::ENVIRONMENT_PROD )
            && ( $clientConfig['request_type'] == self::REQUEST_TYPE_JSON || $clientConfig['request_type'] == self::REQUEST_TYPE_FORM_PARAMS )
        ) {
            self::$user = $clientConfig['user'];
            self::$password = $clientConfig['password'];
            self::$baseUrlDev = $clientConfig['base_url_dev'];
            self::$baseUrlProd = $clientConfig['base_url_prod'];
            self::$responseType = $clientConfig['response_type'];
            self::$apiVersion = $clientConfig['api_version'];
            self::$environment = $clientConfig['environment'];
            self::$requestType = $clientConfig['request_type'];
            self::$basicAuthUsername = $clientConfig['basic_auth_username'];
            self::$basicAuthPassword = $clientConfig['basic_auth_password'];
        } else {
            throw new \Exception('Client API Settings error.');
        }

        if ( $clientConfig['environment'] == self::ENVIRONMENT_PROD ) {
            $baseUrl = $clientConfig['base_url_prod'];
        } else {
            $baseUrl = $clientConfig['base_url_dev'];
        }

        self::setRestApiUrl( $baseUrl );
    }

    /**
     * Config constructor.
     * @throws \Exception
     */
    public function __construct() {
        self::init();
    }

    /**
     * Get request type JSON/FORM_DATA.
     * @return string
     * @throws \Exception
     */
    public static function getRequestType()
    {
        try {
            self::init();
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return self::$requestType;
    }

    /**
     * Get request method POST/GET.
     * @return string
     * @throws \Exception
     */
    public static function getRequestMethod()
    {
        try {
            self::init();
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return self::$requestMethod;
    }

    /**
     * Get Rest API full url without method name.
     * @return string
     * @throws \Exception
     */
    public static function getRestApiUrl()
    {
        try {
            self::init();
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return self::$restApiUrl;
    }

    /**
     * Get response type JSON/XML.
     * @return string
     * @throws \Exception
     */
    public static function getResponseType()
    {
        try {
            self::init();
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return self::$responseType;
    }

    /**
     * Get Rest API user name.
     * @return string
     * @throws \Exception
     */
    public static function getUser()
    {
        try {
            self::init();
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return self::$user;
    }

    /**
     * Get Rest API user password.
     * @return string
     * @throws \Exception
     */
    public static function getPassword()
    {
        try {
            self::init();
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return self::$password;
    }

    /**
     * Get BasicAuth.
     * @return array|bool|string
     * @throws \Exception
     */
    public static function getBasicAuth()
    {
        try {
            self::init();
        } catch (Exception $e) {
            return $e->getMessage();
        }

        if ( !empty(self::$basicAuthUsername) && !empty(self::$basicAuthPassword) ) {
            return [
                self::$basicAuthUsername,
                self::$basicAuthPassword
            ];
        } else return false;
    }

    /**
     * Init settings file path.
     * @param $settingsPath
     * @return string
     * @throws \Exception
     */
    public static function initSettings( $settingsPath )
    {
        self::$settingsPath = $settingsPath;

        try {
            self::init();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Construct Rest API full url without method name.
     * @param $base_url
     */
    protected static function setRestApiUrl( $base_url )
    {
        $configRestApiUrl = $base_url . '/' . 'rest/' . self::$apiVersion . '/';
        self::$restApiUrl = $configRestApiUrl;
    }

    /**
     * Full path to settings Rest API Client file.
     * @throws \Exception
     * @return array
     */
    protected static function getClientConfig()
    {
        return include $_SERVER['DOCUMENT_ROOT'] . self::$settingsPath;
    }

}
