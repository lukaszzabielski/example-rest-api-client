<?php
/**
 * Class ApiRequest
 *
 * @category Monogo
 * @package Monogo\CustomApiClient
 * @author Łukasz Zabielski <lukasz.zabielski@monogo.pl>
 */

namespace monogo\CustomApiClient;

use GuzzleHttp\Client as RAClient;
use monogo\CustomApiClient\Helper\Config as RAConfig;

class ApiRequest
{
    /**
     * Rest API token (JWT).
     * @var null
     */
    protected $token = null;

    /**
     * Request time interval.
     * @var null
     */
    protected $requestTimeInterval = null;

    public $response;

    /**
     * ApiRequest constructor.
     * @param $settingsPath
     * @throws \Exception
     */
    function __construct( $settingsPath )
    {
        RAConfig::initSettings( $settingsPath );
    }

    /**
     * Call to Rest API method with params.
     * Params is sometimes optional, default is empty array.
     * @param $methodName
     * @param array $params
     * @return array|\SimpleXMLElement
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function call( $methodName, $params = array() )
    {
        $methodCallFullUrl = RAConfig::getRestApiUrl() . $methodName;
        $requestMethod = RAConfig::getRequestMethod();
        $responseType = RAConfig::getResponseType();
        $requestType = RAConfig::getRequestType();

        if ( is_null($this->token) && ($methodName != RAConfig::LOGIN_METHOD_NAME) ) {
            throw new \Exception('Need to login call and set token.');
        }

        if ( is_null($this->requestTimeInterval) && ($methodName != RAConfig::LOGIN_METHOD_NAME) ) {
            throw new \Exception('Need to login call and set request time interval.');
        }

        $callData = $this->prepareRequestData( $requestType, $responseType, $methodName, $params );

        if ( !is_null($this->requestTimeInterval) ) {
            $milliseconds = ( $this->requestTimeInterval * 1000 );
            usleep($milliseconds);
        }

        $restClient = new RAClient();
        $this->response = $restClient->request( $requestMethod, $methodCallFullUrl, $callData );

        if ( $this->response->getStatusCode() == 401 ) {
            throw new \Exception('Need to define BasicAuth.');
        } else {
            if ( $responseType == RAConfig::RESPONSE_TYPE_XML ) {
                return simplexml_load_string( $this->response->getBody() );
            } else {
                return json_decode ( $this->response->getBody(), true );
            }
        }


    }

    /**
     * Set Rest API token.
     * @param $token
     */
    public function setToken( $token )
    {
        $this->token = $token;
    }

    /**
     * Get Rest API token.
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set request time interval (millisecond).
     * @param $requestTimeInterval
     */
    public function setRequestTimeInterval( $requestTimeInterval )
    {
        $this->requestTimeInterval = $requestTimeInterval;
    }

    /**
     * Get request time interval in milliseconds.
     * @return int
     */
    public function getRequestTimeInterval()
    {
        return $this->requestTimeInterval;
    }

    /**
     * Get response status code.
     * @return int
     */
    public function getStatusCode()
    {
        return $this->response->getStatusCode();
    }

    /**
     * Prepare request data.
     * @param $request_type
     * @param $response_type
     * @param $method_name
     * @param $params
     * @return array
     * @throws \Exception
     */
    protected function prepareRequestData( $request_type, $response_type, $method_name, $params )
    {
        $callData = [
            'http_errors' => false,
            'read_timeout' => 20,
        ];

        if ( $method_name == RAConfig::LOGIN_METHOD_NAME ) {
            $loginData = [
                'user' => RAConfig::getUser(),
                'password' => RAConfig::getPassword()
            ];

            $params = $loginData;
        }

        if ( !empty($params) ) {
            if ( $request_type == RAConfig::REQUEST_TYPE_JSON ) {
                $callData['json'] = $params;
            } else {
                $callData['form_params'] = $params;
            }
        }

        if ( $response_type == RAConfig::RESPONSE_TYPE_XML ) {
            if ( $method_name != RAConfig::LOGIN_METHOD_NAME ) {
                $callData['headers'] = [
                    'response' => RAConfig::RESPONSE_TYPE_XML,
                    'token' => $this->token
                ];
            } else {
                $callData['headers'] = [
                    'response' => RAConfig::RESPONSE_TYPE_XML,
                ];
            }
        } else {
            if ( $method_name != RAConfig::LOGIN_METHOD_NAME ) {
                $callData['headers'] = [
                    'token' => $this->token
                ];
            }
        }

        $basicAuth = RAConfig::getBasicAuth();
        if ( $basicAuth ) {
            $callData['auth'] = $basicAuth;
        }

        return $callData;
    }

}
