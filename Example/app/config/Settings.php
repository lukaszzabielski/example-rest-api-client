<?php

/**
 * Rest API Client Settings array.
 * Define Your API settings in this file.
 */
return [
    'user' => '', // define your API user name
    'password' => '', // define your API password
    'base_url_dev' => '', // test environment base url "http://test.server"
    'base_url_prod' => '', // production environment base url "http://production.server"
    'response_type' => 'json', // default "json", optional "xml", default "json"
    'api_version' => 'v1', // "v1", default "v1"
    'environment' => 'dev', // "prod|dev", default "dev"
    'request_type' => 'form_params', // "json|form_params", default "form_params"
    // define only if server is secured with BasicAuth, possible in "dev" environment - if empty params not use in call
    'basic_auth_username' => '', // server BasicAuth username
    'basic_auth_password' => '' // server BasicAuth password
];
