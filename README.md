# Rest API Client
Example Rest API Client for dropshipping platform.
1. Clone this package and run composer install.
2. Define API settings.
3. Call to API methods.

```
// RESPONSE JSON EXAMPLE - YOU NEED CREATE YOUR LOGIC

// 1. Init settings file
$request = new ApiRequest('Example/app/config/Settings.php');

// 2. Need to login call
$responseLogin = $request->call('apiLogin');
if ( array_key_exists('error', $responseLogin) ) {
    // print error array
    print_r( $responseLogin );
} else {
    $token = $responseLogin['data'][0]['token'];
    $requestInterval = $responseLogin['data'][1]['request_interval'];

    // need to set token and current request time interval
    $request->setToken( $token );
    $request->setRequestTimeInterval( $requestInterval );
}

/**
 * 3. You have to call all methods
 *
 * Example call with params:
 * $request->call('getProducts', [
 *      'after_date' => '1970-01-01'
 * ]);
 */
$responseGetProducts = $request->call('getProducts');
if ( array_key_exists('error', $responseGetProducts) ) {
    if ( $request->getStatusCode() == '403' ) {
        // try login again, set token and request time interval
    } else {
        // save error logs in to Your database
        print_r( $responseGetProducts );
    }
    print_r( $request->getStatusCode() );
} else {
    // save products data in to Your database
    print_r( $responseGetProducts );
}
```